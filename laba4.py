# -*- coding: utf-8 -*-

import vk_api
import viginer as v, caesar as c
import qrcode
from PIL import Image
from vk_api.longpoll import VkLongPoll, VkEventType

vk = vk_api.VkApi(token='1de45b5216b95c434e51b4d22474412087c196d6acf2553b449c81acb47574f4ba7cec66a151d0034f41b')

STEP_MAIN = 0
STEP_TYPE = 1
STEP_CIPHER_TYPE = 2
STEP_CIPHER_KEY = 3
STEP_CIPHER_TEXT = 4
STEP_QRCODE = 5

class user:
    uid = 0
    step = 0
    atype = 0 # 0 - caesar, 1 - viginer, 2 - QR-Code
    crypt = False # True = crypt, False = decrypt
    key = ''
    def __init__(self, user_id):
        self.uid = user_id

def send_msg(user_id, msg_txt='', msg_photos=[]):
    if len(msg_photos) > 0:
        upload = vk_api.VkUpload(vk)
        uploaded_photos = upload.photo_messages(msg_photos)
        attachments = []
        for photo in uploaded_photos:
            attachments.append('photo{}_{}'.format(photo['owner_id'], photo['id']))
        vk.method('messages.send', {'user_id':user_id, 'attachments':','.join(attachments), 'message':msg_txt})
    else:
        vk.method('messages.send', {'user_id':user_id, 'message':msg_txt})

def main():
    longpoll = VkLongPoll(vk)
    userList = []
	
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                idx = -1

                for i in range(0, len(userList)):
                    if userList[i].uid == event.user_id:
                        idx = i
                        break
                    
                if idx == -1:
                    idx = len(userList)
                    userList.append(user(event.user_id))

                if userList[idx].step == STEP_MAIN:
                    send_msg(event.user_id, 'Выберите, что вы хотите сделать:\n1 - шифр Цезаря;\n2 - шифр Вижинера;\n3 - генерация QR-кода')
                    userList[idx].step = STEP_TYPE

                elif userList[idx].step == STEP_TYPE:
                    cipherid = 0
                    if event.text.isnumeric():
                        cipherid = int(event.text)
                    if cipherid in range(1,4):
                        userList[idx].atype = cipherid
                        ans = ''
                        if cipherid == 1: ans = 'Вы выбрали шифр Цезаря.'
                        elif cipherid == 2: ans = 'Вы выбрали шифр Вижинера.'
                        else: ans ='Вы выбрали генерацию QR-кода.\nВведите текст, который необходимо преобразовать:'
                        
                        if cipherid > 2:
                            userList[idx].step = STEP_QRCODE
                        else:
                            ans = ans + '\nКакую операцию следует выполнить?\n1 - шифрование;\n2 - дешифрование'
                            userList[idx].step = STEP_CIPHER_TYPE
                        send_msg(event.user_id, ans)
                    else:
                        send_msg(event.user_id, 'Такого действия не существует. Повторите попытку:\n1 - шифр Цезаря;\n2 - шифр Вижинера;\n3 - генерация QR-кода')

                elif userList[idx].step == STEP_CIPHER_TYPE:
                    actionid = 0
                    if event.text.isnumeric():
                        actionid = int(event.text)
                    if actionid in range(1,3):
                        ans = ''
                        if actionid == 1:
                            userList[idx].crypt = True
                            ans ='Вы выбрали операцию шифрования.'
                        else:
                            userList[idx].crypt = False
                            ans = 'Вы выбрали операцию дешифрования'
                        if userList[idx].atype == 1:
                            userList[idx].step = STEP_CIPHER_TEXT
                            ans = ans + '\nВведите текст, который необходимо преобразовать:'
                        else:
                            userList[idx].step = STEP_CIPHER_KEY
                            ans = ans + '\nВведите ключ, который необходимо использовать при преобразовании:'
                        send_msg(event.user_id, ans)
                    else:
                        send_msg(event.user_id, 'Такого действия не существует. Повторите попытку:\n1 - шифрование;\n2 - дешифрование')

                elif userList[idx].step == STEP_CIPHER_KEY:
                    if len(event.text) > 0:
                        userList[idx].step = STEP_CIPHER_TEXT
                        userList[idx].key = event.text
                        send_msg(event.user_id, 'Вы ввели ключ. Введите текст, который необходимо преобразовать:')
                    else:
                        send_msg(event.user_id, 'Произошла ошибка. Повторите попытку ввода текста-ключа:')

                elif userList[idx].step == STEP_CIPHER_TEXT:
                    if len(event.text) > 0:
                        ans = ''
                        if userList[idx].atype == 1:
                            if userList[idx].crypt == True: ans = c.encrypt(event.text)
                            else: ans = c.decrypt(event.text)
                        else:
                            if userList[idx].crypt == True: ans = v.encrypt(event.text, userList[idx].key)
                            else: ans = v.decrypt(event.text, userList[idx].key)    
                        send_msg(event.user_id, 'Текст успешно преобразован:\n' + ans)
                        del userList[idx]
                    else:
                        send_msg(event.user_id, 'Произошла ошибка. Повторите попытку ввода текст, небходимого для преобразования:')

                elif userList[idx].step == STEP_QRCODE:
                    if len(event.text) > 0:
                        img = qrcode.make(event.text)
                        img.save('qrphoto.png')
                        img = Image.open('qrphoto.png')
                        send_msg(event.user_id, 'Текст успешно преобразован:', 'qrphoto.png')
                        del userList[idx]
                    else:
                        send_msg(event.user_id, 'Произошла ошибка. Повторите попытку ввода текст, небходимого для преобразования:')

if __name__ == '__main__':
    main()
